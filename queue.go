package generics

type Queue[T any] []T

func (q *Queue[T]) Push(val T) {
	(*q) = append((*q), val)
}

func (q *Queue[T]) Pop() T {
	val := (*q)[0]
	(*q) = (*q)[1:]
	return val
}

func (q *Queue[T]) Size() int64 {
	return int64(len(*q))
}
