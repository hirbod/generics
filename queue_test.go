package generics

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStringQueue(t *testing.T) {
	var queue Queue[string]

	_, ok := any(queue).(Queue[string])
	if !ok {
		t.Fatalf("queue is not []string. got=%T", queue)
	}

	testCases := []struct {
		input   []string
		output  []string
		maxSize int64
	}{
		{
			[]string{"val1", "val2"},
			[]string{"val1", "val2"},
			2,
		},
		{
			[]string{"val1", "val2", "val3", "val4"},
			[]string{"val1", "val2", "val3", "val4"},
			4,
		},
	}

	for _, tt := range testCases {
		queue = Queue[string]{}
		for _, input := range tt.input {
			queue.Push(input)
		}

		assert.Equal(t, tt.maxSize, queue.Size(),
			fmt.Sprintf("invalid maxSize. expected: %v | got: %v", tt.maxSize, queue.Size()))

		for idx, output := range tt.output {
			popped := queue.Pop()
			assert.Equal(t, output, popped,
				fmt.Sprintf("failed to assertEqual %v | output: %v | queue.Pop: %v", idx, output, popped))
		}
	}
}

func TestIntQueue(t *testing.T) {
	var queue Queue[int64]

	_, ok := any(queue).(Queue[int64])
	if !ok {
		t.Fatalf("queue is not []int64. got=%T", queue)
	}

	testCases := []struct {
		input   []int64
		output  []int64
		maxSize int64
	}{
		{
			[]int64{1, 2, 3, 4},
			[]int64{1, 2, 3, 4},
			4,
		},
	}

	for _, tt := range testCases {
		queue = Queue[int64]{}
		for _, input := range tt.input {
			queue.Push(input)
		}

		assert.Equal(t, tt.maxSize, queue.Size(),
			fmt.Sprintf("invalid maxSize. expected: %v | got: %v", tt.maxSize, queue.Size()))

		for idx, output := range tt.output {
			popped := queue.Pop()
			assert.Equal(t, output, popped,
				fmt.Sprintf("failed to assertEqual %v | output: %v | queue.Pop: %v", idx, output, popped))
		}
	}
}
