package generics

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStringStack(t *testing.T) {
	var stack Stack[string]

	_, ok := any(stack).(Stack[string])
	if !ok {
		t.Fatalf("stack is not []string. got=%T", stack)
	}

	testCases := []struct {
		input   []string
		output  []string
		maxSize int64
	}{
		{
			[]string{"val1", "val2"},
			[]string{"val2", "val1"},
			2,
		},
		{
			[]string{"val1", "val2", "val3", "val4"},
			[]string{"val4", "val3", "val2", "val1"},
			4,
		},
	}

	for _, tt := range testCases {
		stack = Stack[string]{}
		for _, input := range tt.input {
			stack.Push(input)
		}

		assert.Equal(t, tt.maxSize, stack.Size(),
			fmt.Sprintf("invalid maxSize. expected: %v | got: %v", tt.maxSize, stack.Size()))

		for idx, output := range tt.output {
			top := stack.Top()
			assert.Equal(t, output, top,
				fmt.Sprintf("failed to assertEqual %v | output: %v | stack.Top: %v", idx, output, top))

			pop := stack.Pop()
			assert.Equal(t, output, pop,
				fmt.Sprintf("failed to assertEqual %v | output: %v | stack.Pop: %v", idx, output, pop))
		}
	}
}

func TestIntstack(t *testing.T) {
	var stack Stack[int64]

	_, ok := any(stack).(Stack[int64])
	if !ok {
		t.Fatalf("stack is not []int64. got=%T", stack)
	}

	testCases := []struct {
		input   []int64
		output  []int64
		maxSize int64
	}{
		{
			[]int64{1, 2, 3, 4},
			[]int64{4, 3, 2, 1},
			4,
		},
	}

	for _, tt := range testCases {
		stack = Stack[int64]{}
		for _, input := range tt.input {
			stack.Push(input)
		}

		assert.Equal(t, tt.maxSize, stack.Size(),
			fmt.Sprintf("invalid maxSize. expected: %v | got: %v", tt.maxSize, stack.Size()))

		for idx, output := range tt.output {
			top := stack.Top()
			assert.Equal(t, output, top,
				fmt.Sprintf("failed to assertEqual %v | output: %v | stack.Top: %v", idx, output, top))

			pop := stack.Pop()
			assert.Equal(t, output, pop,
				fmt.Sprintf("failed to assertEqual %v | output: %v | stack.Pop: %v", idx, output, pop))
		}
	}
}
