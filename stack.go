package generics

type Stack[T any] []T

func (s *Stack[T]) Push(val T) {
	(*s) = append((*s), val)
}

func (s *Stack[T]) Top() T {
	return (*s)[len(*s)-1]
}

func (s *Stack[T]) Pop() T {
	val := (*s)[len(*s)-1]
	(*s) = (*s)[:len(*s)-1]
	return val
}

func (s *Stack[T]) Size() int64 {
	return int64(len(*s))
}
